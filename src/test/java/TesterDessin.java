/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Dimension;
import java.awt.Point;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Seif
 */
public class TesterDessin {
     
    public TesterDessin() {
        
    }

    
    @Test
   public void  testerrect()
   {
       Dessiner ds=new Dessiner();
       ds.DrowRectangle(5,6,7,8);
       Rectangles r=new Rectangles(5,6,7,8);
       assertEquals(r.a,ds.d.rec.a,0.1);
       assertEquals(r.b,ds.d.rec.b,0.1);
       assertEquals(r.x,ds.d.rec.x,0.1);
       assertEquals(r.y,ds.d.rec.y,0.1);
   }
   @Test
   public void  testercercle()
   {
       Dessiner ds=new Dessiner();
       ds.DrowCercle(5,6,7);
       Cercle r=new Cercle(5,6,7);
       assertEquals(r.rayon,ds.d.cer.rayon,0.1);
       assertEquals(r.x,ds.d.cer.x,0.1);
       assertEquals(r.y,ds.d.cer.y,0.1);
   }
   @Test
   public void testerModifrect()
   {
       double a=5,b=6,d=7,e=8;
       Rectangles r=new Rectangles(a,b,d,e);
       r.Modifier(8,7,6,5);
       assertEquals(r.a,6,0.1);
       assertEquals(r.b,5,0.1);
       assertEquals(r.x,8,0.1);
       assertEquals(r.y,7,0.1);
   }
   @Test
   public void testerModifcercle()
   {
       double a=5,b=6,d=7;
       Cercle r=new Cercle(a,b,d);
       r.Modifier(8,7,6);
       assertEquals(r.rayon,6,0.1);
       assertEquals(r.x,8,0.1);
       assertEquals(r.y,7,0.1);
   }
   @Test
   public void testerModifdessin()
   {
       Dessiner d=new Dessiner();
       d.DrowRectangle(5, 6, 7, 8);
       d.ModifierDessin(50,60, 70, 80);
       assertEquals(d.al.get(0).rec.a,70,0.1);
       assertEquals(d.al.get(0).rec.b,80,0.1);
       assertEquals(d.al.get(0).rec.x,50,0.1);
       assertEquals(d.al.get(0).rec.y,60,0.1);
   }
}
